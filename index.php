<?php include "include/query.php"; ?>
<!DOCTYPE html>
<html lang="it-IT">
<head>
	
	<!-- META
	================================================== -->
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=8,9" ><![endif]-->
	<meta name="description" content="TEST ONLINE">
	<meta name="author" content="Maurizio Battaghini">
	
	<!-- WEBSITE ICO
	================================================== -->
	<link rel="icon" href="ico/standard.gif" sizes="16x16" type="image/gif"> 
    <link rel="icon" href="ico/iphone.png"   sizes="57x57" type="image/png"> 
    <link rel="icon" href="ico/vector.svg"   sizes="any"   type="image/svg+xml">
	
	<!-- MOBILE SPECIFIC
	================================================== -->
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	
	<!-- CSS
	================================================== -->	
	<link href="css/main.css" rel="stylesheet" type="text/css">
	
	<link href="css/mobile_portrait.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 320px) and (orientation: portrait)">
	<link href="css/mobile_landscape.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 480px) and (orientation: landscape)">
	<link href="css/tablet.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 769px)">
	<link href="css/netbook.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 1024px)">
	<link href="css/desktop.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 1224px)">
	
	<!-- CSS (vendors)
	================================================== -->	
	<link href="css/vendor/jquery.bxslider.css" rel="stylesheet" type="text/css">
	<link href="js/vendor/select2/select2.css" rel="stylesheet" type="text/css">
	
	
	<!-- Basic Page Needs
	================================================== -->
	<title>TESTING</title>
	
</head>
<body>
	
	<!-- HEADER -->
			<header>
				<div class="header-sx">
					<div class="sx-wrapper">
						<img alt="ROBI" src="img/head_title.png">
						<hgroup>
							<h1>arriva anche in italia!</h1>
							<h3>Partecipa al concorso e vinci un vero Robi!</h3>
						</hgroup>
						<a href="#"><div class="button">iscriviti ora</div></a>
						<a href="#"><img alt="DeAgostini" src="img/logo_normal.jpg"></a>
					</div>
				</div>
				<div class="header-dx">
					<div class="dx-wrapper">
						<img alt="ROBI" src="img/head_robi.png" width="625px" height="590px">
					</div>
				</div>	
			</header>

	<div class="main-container">
		
		<!-- // START PAGE STRUCTURE  -->
		
		
				
		<!-- MENU -->
			<div class="head-menu">
				<nav>
			    	<ul>
			    		<li><a href="#">video</a></li>
			    		<li><a href="#">gallery</a></li>
			    		<li><a href="#">notizie</a></li>
			    		<li><a href="#">feed</a></li>
			    		<li><a href="#">intervista</a></li>
			    		<li><a href="#">concorso</a></li>
			    	</ul>
				</nav>
			</div>
		
		<!-- BODY -->
			<div class="body-wrapper">
				<div class="body-container">
					
					<!-- SLIDER -->
					<section id="anchor-video" class="slider">
						<div class="content slider">
							<div class="body-sx">
								<hgroup>
									<h1>konnichiwa!</h1>
									<h2>ciao, io sono robi</h2>
								</hgroup>
								<p>
									Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è 									considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una 									cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a ptiù di 									cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato.
									<br><br>
									Fu reso popolare, negli anni ’60, con la diffusione dei fogli di caratteri trasferibili “Letraset”, che 									contenevano passaggi del Lorem
								</p>
							</div>
							<div class="body-dx">
								
								<ul class="bxslider">
									<?php
										// STAMPA RISULTATI
										stampa_dati();
									?>	
								</ul>
								
							</div>
						</div>
					</section>
					
					<!-- MODEL -->
					<section id="anchor-gallery" class="model">
						<div class="content model">
							<!-- controllo modello -->
							<div class="model-controllers">
								<nav>
									<ul>
										<li><a class="active" href="#">dietro</a></li>
										<li><a href="#">davanti</a></li>
									</ul>
								</nav>
							</div>
							<!-- container modello -->
							<div class="model-container back">
								<div class="back-robi model-container-robi">
									<div class="spot-detail back1"></div>
									<div class="spot-detail back2"></div>
									<div class="spot-detail back3"></div>
								</div>
								
								<!-- box 1 -->
								<div class="container-detail detail1">
									<div class="box">
										<hgroup>
											<h5>Titolo del post n. 1</h5>
										</hgroup>
										<p>
											I sensori rilevano un soffio di persone, girare la faccia in risposta al movimento di persone.
										</p>
										<div class="linkto"><a href="#">guarda il video <span class="link-triangle"></span></a></div>
									</div>
								</div>
								
								<!-- box 2 -->
								<div class="container-detail detail2">
									<div class="box">
										<hgroup>
											<h5>Titolo del post n. 2</h5>
										</hgroup>
										<p>
											I sensori rilevano un soffio di persone, girare la faccia in risposta al movimento di persone.
										</p>
										<div class="linkto"><a href="#">guarda il video <span class="link-triangle"></span></a></div>
									</div>
								</div>
								
								<!-- box 3 -->
								<div class="container-detail detail3">
									<div class="box">
										<hgroup>
											<h5>Titolo del post n. 3</h5>
										</hgroup>
										<p>
											I sensori rilevano un soffio di persone, girare la faccia in risposta al movimento di persone.
										</p>
										<div class="linkto"><a href="#">guarda il video <span class="link-triangle"></span></a></div>
									</div>
								</div>
								
							</div>
							<!-- CHIUDO: model-container BACK -->
							
							<div class="model-container front">
								<div class="front-robi model-container-robi">
									<div class="spot-detail front4"></div>
									<div class="spot-detail front5"></div>
								</div>
								
								<!-- box 4 -->
								<div class="container-detail detail4">
									<div class="box">
										<hgroup>
											<h5>Titolo del post n. 4</h5>
										</hgroup>
										<p>
											I sensori rilevano un soffio di persone, girare la faccia in risposta al movimento di persone.
										</p>
										<div class="linkto"><a href="#">guarda il video <span class="link-triangle"></span></a></div>
									</div>
								</div>
								
								<!-- box 5 -->
								<div class="container-detail detail5">
									<div class="box">
										<hgroup>
											<h5>Titolo del post n. 5</h5>
										</hgroup>
										<p>
											I sensori rilevano un soffio di persone, girare la faccia in risposta al movimento di persone.
										</p>
										<div class="linkto"><a href="#">guarda il video <span class="link-triangle"></span></a></div>
									</div>
								</div>
								
							</div>
							<!-- CHIUDO: model-container FRONT -->
														
						</div>
						<!-- CHIUDO: content-model -->
						
						<div class="text-img">
							<div class="box textleft">
								<hgroup class="dark-title">
									<h2>comportamento naturale</h2>
								</hgroup>
								<p>
									Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo.
									<br><br>
									Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo.
								</p>
							</div>
							<img alt="COMPORTAMENTO NATURALE" src="img/robi/move_robi.png"/>
						</div>
						<!-- CHIUDO: text-imgright -->
						
						<div class="body-full">	
							<ul class="bxslider">
								<?php
									// STAMPA RISULTATI
									stampa_dati2();
								?>
							</ul>
						</div>
						<!-- CHIUDO: gallery2 -->
						
					</section>
					<!-- CHIUDO: model -->
					
					<section id="anchor-notizie" class="hellorobi">
						<div class="hellorobi-split sx">
							<div class="hellorobi-title">
								<img alt="HELLO ROBI" src="img/hellorobi.png">
							</div>
							<div class="hellorobi-wrapper">
								<img alt="ROBI" src="img/robi/hellorobi.png" />
							</div>
						</div>
						<!-- CHIUDO: split SX -->
						<div id="article" class="hellorobi-split dx">
							<div class="searchbar">
								<input class="search" placeholder="Search" />
							</div>
							<?php 
								// STAMPA RISULTATI
								stampa_dati3();
							?>
						</div>	
						<div class="separator"/>&nbsp;</div>
						<!-- CHIUDO: list -->
						<div id="anchor-feed" class="content hellorobi">
							<div>
								<hgroup>
									<h1>&#35;hellorobi</h1>
									<h4>partecipa alla conversazione con l'hashtag di Robi</h4>
								</hgroup>
							</div>												
						</div>			
						
						<div class="conversazione-wrapper">
							<div class="conversazione-container">
								<ul class="conversazione-list">
									<!-- stampa dati -->
								</ul>
								<div class="loadmore button">carica</div>
							</div>								
						</div>
						<!-- CHIUDO: conversazione -->
										
					</section>
					<!-- CHIUDO: hellorobi -->
					
					<!-- CREATORE -->
					<section id="anchor-intervista" class="creatore">
						<div class="content creatore">
							<div class="creatore-sx">
								<div class="sx-wrapper">
									<hgroup>
										<h1>tomotaka takahashi</h1>
										<h2>creatore di robi</h2>
									</hgroup>
									<p>
										Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a ptiù di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato. 
									</p>
									<div class="button">leggi di più!</div>
								</div>
							</div>
							<div class="creatore-dx">
								<div class="dx-wrapper">
									<img alt="TOMOTAKA TAKAHASHI" src="img/creatore.png" width="528px" height="628px">
								</div>
							</div>
						</div>
					</section>
					<!-- CHIUDO: creatore -->
					
					<!-- CREATORE -->
					<section id="anchor-concorso" class="form">
						<div class="content form">
							
							<div class="form-wrapper">
								<hgroup>
									<h1>iscriviti ora!</h1>
									<h2>non perdere l'occasione di avere il tuo robi</h2>
								</hgroup>
								<form id="contactform" action="include/contactform.php" method="post">
									<fieldset>
										<span>indirizzo mail</span>
										<input class="input-uno" id=email name=email type=email placeholder="Inserisci indirizzo e-mail valido" required>
									</fieldset>
									<fieldset>
										<span>il tuo nome</span>
										<input class="input-due" id=cognome name=nome minlength="3" type=text placeholder="Nome" required>
										<input class="input-due" id=cognome name=cognome minlength="3" type=text placeholder="Cognome" required>
									</fieldset>
									<fieldset>
										<span>data di nascita</span>
										<select id="giorno" class="required" name="giorno" required>
											<option value="">Giorno</option>
											<option value="01">1</option>
											<option value="02">2</option>
											<option value="03">3</option>
										</select>
										<select id="mese" class="required" name="mese" required>
											<option value="">Mese</option>
											<option value="01">gennaio</option>
											<option value="02">febbraio</option>
											<option value="03">marzo</option>
										</select>
										<select id="anno" class="required" name="anno" required>
											<option value="">Anno</option>
											<option value="1989">1989</option>
											<option value="1990">1990</option>
											<option value="1991">1991</option>
										</select> 
									</fieldset>	
									<fieldset>
										<span>codice di sicurezza</span>
										<input class="input-due" id=codice minlength="5" name=codice type=text placeholder="Codice di sicurezza" required>
									</fieldset>
									<fieldset>
									  <input class="button submit-button" type=submit value="iscriviti ora">
									  <div id="risposta"><!-- --></div>
									</fieldset>								
								</form>
							</div>
							
						</div>
					</section>
					<!-- CHIUDO: form -->
					
					
										
					
				</div> <!-- CHIUDO: body-container -->
			</div> <!-- CHIUDO: body-wrapper -->
		
		<!-- FOOTER -->
		<footer>
			<div class="footer-wrapper">
				<div class="footer-sx">
					<a href="#"><img alt="DeAgostini" src="img/logo_small.jpg"/></a>
				</div>
				<div class="footer-dx">
					<nav>
						<ul>
							<li><a href="#">Informazioni</a></li>
							<li><a href="#">Privacy</a></li>
							<li><a href="#">Condizioni</a></li>
							<li><a href="#">Centro Assistenza</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</footer>
		<!-- // END PAGE STRUCTURE -->
	
	</div> <!-- CHIUDO: main-container -->	

	<!-- JS
	================================================== -->
	<script src="js/vendor/responde-min.js" type="text/javascript"></script>
	<script src="js/vendor/modernizr.js" type="text/javascript"></script>
	<script src="js/vendor/jquery-1.8.3.min.js" type="text/javascript"></script>
	<script src="js/vendor/css3-mediaqueries.js" type="text/javascript"></script>
	<script src="js/vendor/selectivizr-min.js" type="text/javascript"></script>
	<script src="js/vendor/html5shiv.js" type="text/javascript"></script>
	<script src="js/vendor/jquery.bxslider.min.js" type="text/javascript"></script>
	<script src="js/vendor/scrollTo.js" type="text/javascript"></script>
	<script src="js/vendor/list.js" type="text/javascript"></script>
	<script src="js/vendor/jquery.validate.min.js"text/javascript"></script>
	<script src="js/vendor/localization/messages_it.js"text/javascript"></script>
	<script src="js/vendor/jquery.form.js"text/javascript"></script>
	<script src="js/vendor/select2/select2.min.js"text/javascript"></script>
	
	<!-- PLUGIN
	================================================== -->
	<script src="js/plugin/plugin.twitterfeed.js" type="text/javascript"></script>
	
	<!-- SCRIPTS
	================================================== -->
	<script src="js/main.js" type="text/javascript"></script>

</body>
</html>

<?php
	// CHIUDE CONNESSIONE DB
	mysql_close($db_connection);
?>
