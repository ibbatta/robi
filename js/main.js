/* ================================
 * GLOBAL
 * ================================ */
// TIMING
var quarterSec = 250;
var halfSec = 500;
var oneSec = 1000;

var conversazioneCounter;
var data_from, data_to;


/* ================================
 * FUNCTIONS
 * ================================ */
function loadImg()
{
	$("img").load(function(){
  		$(this).animate({
  			"opacity":1
  		}, 300);
  	}).each(function(){
  		if(this.complete){
  			// controlla che le immagini non siano in cache
  			$(this).trigger("load");
  		}
  	});
}

function caricaConversazioni()
{
	$(".conversazione-element").stop().animate({
		"opacity":1
	}, halfSec, function(){
		$(".loadmore").text("carica");
	});
}

function loadQuery(data_from, data_to)
{
	$.ajax({
		type: "POST",
		url: "include/conversazioni.php",
		data: { "query_from": data_from, "query_to": data_to }
	}).done(function(msg) {
		if(msg != "")
		{
			$("ul.conversazione-list").append(msg);
			caricaConversazioni();
			loadImg();
		}
		else
		{
			alert("non ci sono elementi");	
		}
		
	});
}



/* ================================
 * SCRIPT PAGINA
 * ================================ */
$(function() {
	
	// SELECT2 - personalizzazione select
		$("select").select2();
		
  	// FUNZIONE LOAD IMMAGINI
	  	loadImg();
	  	  
	// RICHIAMA SLIDER
		$('.bxslider').bxSlider({
	 		infiniteLoop: false,
	  		hideControlOnEnd: true
		});
	
	// MODEL CONTROLLER
		// active menu
			$(".model-controllers a").live("click", function(e){
				e.preventDefault();
				$(".model-controllers a").removeClass("active");
				$(this).addClass("active");
				if($(".model-controllers li:eq(0) a").hasClass("active")) {
					$(".front").stop().fadeOut(quarterSec, function(){
						$(".back").stop().fadeIn(quarterSec);
					});	
				}
				else {
					$(".back").stop().fadeOut(quarterSec, function(){
						$(".front").stop().fadeIn(quarterSec);
					});
				}
			});
			
	// SPOT DETAIL
		$(".spot-detail").click(function(){
			$(".container-detail").stop().fadeOut(0);
			$(".spot-detail").removeClass("active");
			
			if(!$(this).hasClass("active"))
			{
				var actualElement = $(this).attr("class").slice(-1);
				$(".detail"+actualElement).stop().fadeIn(quarterSec);
				$(this).addClass("active");
			}		
		});
		
	// SEARCH ARTICLE
		var options = {
		  valueNames: [ 'title', 'text' ]
		};
		
		var userList = new List('article', options);
		
	// CARICA CONVERSAZIONI
		loadQuery(0, 8);
		
		
	// LOADMORE CONVERSAZIONI
		$(".loadmore").on("click", function(){	
			$(".loadmore").text("caricamento ...");
			conversazioneCounter = $("ul.conversazione-list li").size();	
			loadQuery(conversazioneCounter, 4);
			caricaConversazioni();
		});
		
		
	// VALIDAZIONE FORM
		$("#contactform").validate({
			submitHandler: function(form) {
				var queryString = $('#contactform').formSerialize(); 
				$.ajax({
					type: "POST",
					url: "include/contactform.php",
					data: queryString
				}).done(function(msg) {
					if(msg != "")
					{
						$("#risposta").append(msg);
						$(".messageBack").fadeIn(oneSec, function(){
							$(this).fadeOut(oneSec, function(){
								$(this).remove();
							});	
						});
					}
				}); 
				
				return false;	
			}
		});
		
	// SCROLL TO
		$(".head-menu nav ul li a").live("click", function(e){
			e.preventDefault();
			var linkTarget  = $(this).text();
			$("body").scrollTo($("#anchor-"+linkTarget), oneSec);
		});
		
  
});

