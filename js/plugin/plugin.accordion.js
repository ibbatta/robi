/* =====================================
 * AUTHOR:		Maurizio Battaghini
 * ROLE:		web developer
 * 
 * TITLE:		#accordion
 * 
 * DATE:		28-05-2013
 * ===================================== */
(function($) {
	$.fn.accordion = function(options){
		
		// SETTINGS
		var settings = {
			timeOpen : 400,
			timeClose : 400,
			bgColor: "#C6C6C6",
			txtColor:"#333"
		};
		$.extend(settings, options);
		
		return this.each(function(){
			
			var row = $(this).children("dt");			
			row.click(onClick);
			row.each(initialize);
			
		});
		
		// FUNCTIONS
		function onClick()
		{
			$(this).siblings("dt").each(hide);
			$(this).next().slideDown(settings.timeOpen);
			return false;
		}
		
		function hide()
		{
			$(this).next().slideUp(settings.timeClose);
		}
		
		
		function initialize()
		{
			$(this).next().css({
				"background-color":settings.bgColor,
				"color":settings.txtColor
			});
			$(this).next().slideUp(0);
		}
			
	};
})(jQuery);
