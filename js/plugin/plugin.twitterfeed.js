/* =====================================
 * AUTHOR:		Maurizio Battaghini
 * ROLE:		web developer
 * 
 * TITLE:		#tiwtterfeed
 * 
 * DATE:		24-06-2013
 * ===================================== */
(function($) {
	$.fn.tweetfeed = function(options){
		
		// SETTINGS
		var settings = {
			displayname : '',
			displaytweets : 3,
			intervalTime:15
		};
		$.extend(settings, options);
		
		return this.each(function(){			
			
			// Prima chiamata api
			twitterApiCall(settings.displayname, settings.displaytweets);
			
			// Chiamate api temporalizzate
			if(settings.intervalTime > 0)
			{
				setInterval(function() {
				      // Do something after 10 seconds
				      
				      twitterApiCall(settings.displayname, settings.displaytweets);
				      
				}, settings.intervalTime*1000*60);
			}
			else
			{
				console.log("non effettua chiamate ripetute");
			}
			
			
		});
		
			
			
			// API CALL
			function twitterApiCall(username, tweets)
			{
				$.post("php/twitter_feed.php", {'displayname': username, 'displaytweets': tweets}, function(data){
					var i = settings.displaytweets-1;
					$("#tweetfeed").text("");
					if($("#tweetfeed div:first-child").attr("id") != data[0].id)
					{
						for(i; i>=0; i--)
						{ 
							$("#tweetfeed").prepend("<div class='twitter_wrap' id='"+data[i].id+"'><a target='_blank' href='http://twitter.com/"+settings.displayname+"/status/"+data[i].id+"'><img class='user_img' src='"+data[i].user.profile_image_url+"'/><p class='user_text'>" + data[i].text + "</p></a></div>");
						}
					}

				}, "json");	
			}
			
	};
})(jQuery);