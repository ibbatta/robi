<?php header('Content-Type: text/html; charset=UTF-8');

include "include/db-connection.php";

	// QUERY - 1
	$query = mysql_query("SELECT * FROM test");
	if (!$query) {
	    die('QUERY [error]: ' . mysql_error() . "<br>");
	}
	
	function stampa_dati(){
		global $query;
		$i = 0;
		while($data = mysql_fetch_assoc($query))
		{
			if($i % 3 == 0){
				echo "<li><div class='slide-wrapper'>";
			}
			echo "<div class='slider_article'><a href='#'><img title='".$data["name"]."' alt='articolo '".$data["id"]."' src='".$data["path"] . "'></a></div>";	
			if($i % 3 == 2){
				echo "</div></li>";
			}
			$i++;
		}
		
		if($i % 3 == 1){
			echo "</div></li>";
		}
	}
	
	
	// QUERY - 2
	$query2 = mysql_query("SELECT * FROM test2");
	if (!$query2) {
	    die('QUERY [error]: ' . mysql_error() . "<br>");
	}
	
	function stampa_dati2(){
		global $query2;
		$i = 0;
		while($data = mysql_fetch_assoc($query2))
		{
			if($i % 3 == 0){
				echo "<li><div class='slide-wrapper_full'>";
			}
			echo "<div class='slider_article_full'><a href='#'><img title='".$data["name"]."' alt='articolo '".$data["id"]."' src='".$data["path"] . "'></a></div>";	
			if($i % 3 == 2){
				echo "</div></li>";
			}
			$i++;
		}
		
		if($i % 3 == 1){
			echo "</div></li>";
		}
	}
	
	
	// QUERY - 3
	$query3 = mysql_query("SELECT * FROM test3");
	if (!$query3) {
	    die('QUERY [error]: ' . mysql_error() . "<br>");
	}
	
	function stampa_dati3(){
		global $query3;
				
		echo '<ul class="article-wrapper list">';
		while ($row = mysql_fetch_array($query3, MYSQL_BOTH))
		{		
			printf('<li><article><div class="article-sx article-content"><div class="article-avatar"><img alt="AVATAR" src="%s"></div></div><div class="article-dx article-content"><h3 class="title">%s</h3><p class="text">%s</p><div class="read-more">leggi di più <span class="logo-link">&nbsp;</span></div></div></article></li>', $row["avatar"], $row["titolo"], $row["testo"]);
		}
		echo '</ul>';
		
		mysql_free_result($query3);
		
	}
		
	
?>